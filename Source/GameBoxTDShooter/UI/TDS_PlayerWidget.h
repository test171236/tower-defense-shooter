// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_PlayerWidget.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOXTDSHOOTER_API UTDS_PlayerWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	virtual void NativeConstruct() override;
	virtual bool Initialize() override;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UProgressBar* HealthBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* GoldNum;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* HintText;
public:
	void Update();
	void ToggleHint();
};
