// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "TDS_PlayerWidget.h"

ATDS_HUD::ATDS_HUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/Objects/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
}


void ATDS_HUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X - CrosshairTex->GetSizeX()/2), 
		(Center.Y - CrosshairTex->GetSizeY() / 2));

	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);

	if (isPrintStatusText)
	{
		PrintCentredMultilineText(StatusText, Center.X, Center.Y + CrosshairTex->GetSizeY());

		isPrintStatusText = false;
	}

	if (isPrintActionText)
	{
		PrintCentredMultilineText(ActionText, Center.X, Canvas->ClipY * 3.f / 4.f);
		
		isPrintActionText = false;
	}

	PlayerWidget->Update();
}

void ATDS_HUD::BeginPlay()
{
	PlayerWidget = CreateWidget<UTDS_PlayerWidget>(GetWorld(), PlayerWidgetClass);
	if (PlayerWidget)
	{
		PlayerWidget->AddToViewport();
	}
}

void ATDS_HUD::DrawStatus(FString Text)
{
	isPrintStatusText = true;
	StatusText = Text;
}

void ATDS_HUD::DrawAction(FString Text)
{
	isPrintActionText = true;
	ActionText = Text;
}

void ATDS_HUD::ToggleHint()
{
	PlayerWidget->ToggleHint();
}

void ATDS_HUD::PrintCentredMultilineText(const FString &Text, float X, float Y)
{
	FString LeftS, RightS(Text);
	float curX = X, curY = Y;

	while (RightS.Split("\n", &LeftS, &RightS))
	{
		float ow, oh;
		GetTextSize(LeftS, ow, oh);
		DrawText(LeftS, FLinearColor::White, curX - ow / 2, curY);
		curY += oh;
	}

	{
		float ow, oh;
		GetTextSize(RightS, ow, oh);
		DrawText(RightS, FLinearColor::White, curX - ow / 2, curY);
	}
}

