// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS_ActorStatus.generated.h"

UINTERFACE(MinimalAPI)
class UTDS_ActorStatus : public UInterface
{
	GENERATED_BODY()
};

class ATDS_HUD;

/**
 * 	 
 */
class GAMEBOXTDSHOOTER_API ITDS_ActorStatus
{
	GENERATED_BODY()

public:

	UFUNCTION()
	virtual void ShowStatus(ATDS_HUD* HUD) const = 0;
	
	UFUNCTION()
	virtual void ShowAction(ATDS_HUD* HUD) const;
	
	UFUNCTION()
	virtual void ActivateAction(APawn *Operator);
};
