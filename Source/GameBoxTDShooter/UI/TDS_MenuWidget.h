// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_MenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOXTDSHOOTER_API UTDS_MenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

	UFUNCTION()
	void ClickExit();
	UFUNCTION()
	void ClickRetry();

	UPROPERTY(EditDefaultsOnly)
	FName LevelName;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* MenuTitle;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* ExitButton;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* RetryButton;

};
