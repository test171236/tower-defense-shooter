// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TDS_HUD.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_HUD : public AHUD
{
	GENERATED_BODY()

	bool isPrintStatusText = false;
	FString StatusText;
	bool isPrintActionText = false;
	FString ActionText;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UTDS_PlayerWidget> PlayerWidgetClass;

	UPROPERTY()
	class UTDS_PlayerWidget* PlayerWidget;

public:
	ATDS_HUD();

	virtual void DrawHUD() override;
	virtual void BeginPlay() override;

	void DrawStatus(FString Text);
	void DrawAction(FString Text);
	
	UFUNCTION()
	void ToggleHint();

private:
	/** �������� ������� */
	UPROPERTY()
	class UTexture2D* CrosshairTex;

	void PrintCentredMultilineText(const FString& Text, float X, float Y);
};
