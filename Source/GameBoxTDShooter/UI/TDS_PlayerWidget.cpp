// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_PlayerWidget.h"

#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Blueprint/WidgetTree.h"
#include "Kismet/GameplayStatics.h"
#include "../Gameplay/TDS_Character.h"

void UTDS_PlayerWidget::NativeConstruct()
{
    Super::NativeConstruct();
}

bool UTDS_PlayerWidget::Initialize()
{
    if (!Super::Initialize())
        return false;

    return true;
}

void UTDS_PlayerWidget::Update()
{
    ATDS_Character* ch = Cast<ATDS_Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

    if (IsValid(ch))
    {
        if (IsValid(HealthBar))
            HealthBar->SetPercent(ch->GetCurrentHP());
        if (IsValid(GoldNum))
        {
            FString s = FString::Printf(TEXT("Gold: %d"), ch->GetCurrentGold());
            GoldNum->SetText(FText::FromString(s));
        }
    }
}

void UTDS_PlayerWidget::ToggleHint()
{
    if(HintText->IsVisible())
        HintText->SetVisibility(ESlateVisibility::Hidden);
    else
        HintText->SetVisibility(ESlateVisibility::Visible);
}

