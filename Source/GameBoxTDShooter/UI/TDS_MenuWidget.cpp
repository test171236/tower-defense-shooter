// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_MenuWidget.h"

#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

bool UTDS_MenuWidget::Initialize()
{
    if (!Super::Initialize())
        return false;

    if (IsValid(ExitButton))
        ExitButton->OnClicked.AddDynamic(this, &UTDS_MenuWidget::ClickExit);

    if (IsValid(RetryButton))
        RetryButton->OnClicked.AddDynamic(this, &UTDS_MenuWidget::ClickRetry);

    return true;
}

void UTDS_MenuWidget::ClickExit()
{
    UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
}

void UTDS_MenuWidget::ClickRetry()
{
    UGameplayStatics::OpenLevel(GetWorld(), LevelName);
}
