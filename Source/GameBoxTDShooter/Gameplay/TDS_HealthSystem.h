// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS_HealthSystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEBOXTDSHOOTER_API UTDS_HealthSystem : public UActorComponent
{
	GENERATED_BODY()

	float MaxHP = 100.f;
	float CurHP = 100.f;
	bool isDead = false;

public:	
	// Sets default values for this component's properties
	UTDS_HealthSystem();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDied);
	UPROPERTY()
	FOnDied	OnDied;

	UFUNCTION()
	void SetMaxHP(float newMaxHP);
	
	UFUNCTION()
	float ReduceHP(float deltaHP);
	
	UFUNCTION()
	void RestoreFullHP();
	
	UFUNCTION()
	float GetCurHP() const;
	UFUNCTION()
	float GetMaxHP() const;
};
