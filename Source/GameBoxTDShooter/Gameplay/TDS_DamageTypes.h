// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "TDS_DamageTypes.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOXTDSHOOTER_API UTDS_DrillDamage : public UDamageType
{
	GENERATED_BODY()
	
};

UCLASS()
class GAMEBOXTDSHOOTER_API UTDS_MeleeDamage : public UDamageType
{
	GENERATED_BODY()

};

UCLASS()
class GAMEBOXTDSHOOTER_API UTDS_HitScanDamage : public UDamageType
{
	GENERATED_BODY()
};

