// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_TurretTargetingSystem.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../Enemies/TDS_EnemyCharacter.h"

ATDS_TurretTargetingSystem::ATDS_TurretTargetingSystem()
{
	PrimaryActorTick.bCanEverTick = false;

	AttackArea = CreateDefaultSubobject<USphereComponent>(TEXT("AttackArea"));
	SetRootComponent(AttackArea);
	AttackArea->SetSphereRadius(Radius);
	AttackArea->SetCollisionProfileName(TEXT("TurretAttackArea"));

	OnActorBeginOverlap.AddDynamic(this, &ATDS_TurretTargetingSystem::ActorBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &ATDS_TurretTargetingSystem::ActorEndOverlap);
}

void ATDS_TurretTargetingSystem::UpdateBestTargetsArray()
{
	BestTargets.Empty();

	for (auto& i : PossibleTargets)
		if (IsValid(i) && !i->IsDead())
			BestTargets.Add(i);

	ACharacter* player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	auto predicate = [player](ATDS_EnemyCharacter* ch1, ATDS_EnemyCharacter* ch2)
		{
			return player->GetDistanceTo(ch1) < player->GetDistanceTo(ch2);
		};

	Algo::Sort(BestTargets, predicate);
}

void ATDS_TurretTargetingSystem::BeginPlay()
{
	Super::BeginPlay();

	//UpdateOverlaps();

	TSubclassOf<ATDS_EnemyCharacter> EnemyClass = ATDS_EnemyCharacter::StaticClass();
	TArray<AActor*> res;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), EnemyClass, res);

	for (auto& i : res)
	{
		if (GetDistanceTo(i) <= Radius)
		{
			ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(i);
			if (IsValid(ch) && !ch->IsDead())
				PossibleTargets.AddUnique(ch);
		}
	}
}

void ATDS_TurretTargetingSystem::ActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(OtherActor);

	if (IsValid(ch))
	{
		PossibleTargets.AddUnique(ch);
	}
}

void ATDS_TurretTargetingSystem::ActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(OtherActor);

	if (IsValid(ch))
	{
		PossibleTargets.Remove(ch);
	}
}

// Called every frame
void ATDS_TurretTargetingSystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

