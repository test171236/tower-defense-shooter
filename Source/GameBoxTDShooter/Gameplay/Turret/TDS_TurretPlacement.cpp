// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_TurretPlacement.h"

// Sets default values
ATDS_TurretPlacement::ATDS_TurretPlacement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_TurretPlacement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_TurretPlacement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

