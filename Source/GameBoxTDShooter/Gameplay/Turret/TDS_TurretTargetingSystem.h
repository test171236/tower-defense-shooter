// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_TurretTargetingSystem.generated.h"

// �������� ����������� ������, �������� � ������ ��������� ������
// �������� �������� � ��������� actor ��-�� ������� � ����������
UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_TurretTargetingSystem : public AActor
{
	GENERATED_BODY()
	
	// ��� ��������� ���� � ������� ���������
	UPROPERTY()
	TArray<class ATDS_EnemyCharacter*> PossibleTargets;

	// ������ ����, ��������������� � ������� �������� ��������
	UPROPERTY()
	TArray<class ATDS_EnemyCharacter*> BestTargets;

	UPROPERTY()
	class USphereComponent* AttackArea;

	float Radius = 700.f;

public:	
	// Sets default values for this actor's properties
	ATDS_TurretTargetingSystem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void ActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	void ActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void UpdateBestTargetsArray();
	const TArray<class ATDS_EnemyCharacter*>& GetBestTargetsArray() const 
	{ return BestTargets; }

	float GetRadius() const { return Radius; }
};
