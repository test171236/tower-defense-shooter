// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_TurretPlacement.generated.h"

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_TurretPlacement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_TurretPlacement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	bool canBePlaced = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	int GetOverlapCount() const;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetCanBePlaced(bool newVal);

	bool GetCanBePlaced() const { return canBePlaced; }
};
