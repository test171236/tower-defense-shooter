// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_TurretPawn.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraSystem.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "../TDS_HealthSystem.h"
#include "../../UI/TDS_HUD.h"
#include "../Enemies/TDS_EnemyCharacter.h"
#include "TDS_TurretTargetingSystem.h"
#include "../TDS_DamageTypes.h"
#include "../../Game/TDS_GameModeBase.h"

ATDS_TurretPawn::ATDS_TurretPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	//SetRootComponent(Root);

	Base = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base"));
	Base->SetCollisionProfileName(TEXT("Turret"));
	Base->SetupAttachment(RootComponent);

	Turret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret"));
	Turret->SetCollisionProfileName(TEXT("NoCollision"));
	Turret->SetupAttachment(RootComponent);

	FirePoint = CreateDefaultSubobject<USceneComponent>(TEXT("FirePoint"));
	FirePoint->SetupAttachment(Turret);

	HealthSystem = CreateDefaultSubobject<UTDS_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(5.f);
	HealthSystem->OnDied.AddDynamic(this, &ATDS_TurretPawn::Death);
	OnTakeAnyDamage.AddDynamic(this, &ATDS_TurretPawn::TakeAnyDamage);
}

void ATDS_TurretPawn::BeginPlay()
{
	Super::BeginPlay();
	
	TargetingSystem = GetWorld()->SpawnActor<ATDS_TurretTargetingSystem>(
		ATDS_TurretTargetingSystem::StaticClass(),
		FTransform(GetActorLocation()));

	ATDS_GameModeBase* gm = Cast<ATDS_GameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
		gm->AddTurret(this);
}

void ATDS_TurretPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	TargetingSystem->Destroy();

	ATDS_GameModeBase* gm = Cast<ATDS_GameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
		gm->RemoveTurret(this);
}

void ATDS_TurretPawn::Death()
{
	Destroy();
}

void ATDS_TurretPawn::TakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	HealthSystem->ReduceHP(Damage);
}

void ATDS_TurretPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(TargetingSystem))
	{
		TargetingSystem->UpdateBestTargetsArray();

		auto& arr = TargetingSystem->GetBestTargetsArray();
		
		CurrentTarget = nullptr;

		FHitResult hr;
		for (int i = 0; i < arr.Num(); i++)
		{
			if (!IsValid(arr[i]) || !IsValid(arr[i]->GetMesh()))
				continue;

			FVector head_loc = arr[i]->GetMesh()->GetSocketLocation("HeadSocket");

			FVector target_dir = 
				head_loc - Turret->GetComponentLocation();

			Turret->SetWorldRotation(target_dir.Rotation());

			FVector start = Turret->GetComponentLocation(),
				end = start + Turret->GetForwardVector() * TargetingSystem->GetRadius();

			UKismetSystemLibrary::LineTraceSingle(
				GetWorld(),
				start,
				end,
				ETraceTypeQuery::TraceTypeQuery1,
				false,
				TArray<AActor*>({ this }),
				EDrawDebugTrace::ForOneFrame,
				hr,
				true
			);

			if (hr.bBlockingHit && hr.Actor == arr[i])
			{
				CurrentTarget = arr[i];
				break;
			}
		}

		float cur_time = GetWorld()->GetTimeSeconds();
		if (cur_time - LastFireTime >= 1.f / FireRate &&
			IsValid(CurrentTarget))
		{
			UGameplayStatics::ApplyPointDamage(
				CurrentTarget,
				ShotDamage,
				Turret->GetForwardVector(),
				hr,
				nullptr,
				this,
				UTDS_HitScanDamage::StaticClass());

			LastFireTime = cur_time;

			UNiagaraComponent* shot = UNiagaraFunctionLibrary::SpawnSystemAtLocation(
				GetWorld(),
				ShotBeam,
				Turret->GetComponentLocation(),
				FRotator::ZeroRotator,
				FVector(1.f),
				true, 
				false,
				ENCPoolMethod::AutoRelease
			);
			shot->SetVectorParameter("EndPoint", hr.ImpactPoint);
			shot->Activate(true);
		}
	}

}

void ATDS_TurretPawn::ShowStatus(ATDS_HUD* HUD) const
{
	if (HUD != nullptr)
	{
		FString s = FString::Printf(TEXT("Turret\nHP: %.2f"), HealthSystem->GetCurHP());
		HUD->DrawStatus(s);
	}
}

