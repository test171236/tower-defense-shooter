// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "../../UI/TDS_ActorStatus.h"
#include "TDS_TurretPawn.generated.h"

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_TurretPawn : public AActor, public ITDS_ActorStatus
{
	GENERATED_BODY()

	UPROPERTY()
	class UTDS_HealthSystem* HealthSystem;

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* Base;
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* Turret;

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* FirePoint;

	UPROPERTY(EditDefaultsOnly)
	class UNiagaraSystem *ShotBeam;

	UPROPERTY()
	class ATDS_TurretTargetingSystem* TargetingSystem;

	UPROPERTY()
	class ATDS_EnemyCharacter* CurrentTarget = nullptr;

	float FireRate = 2.f;
	float LastFireTime = -10000.f;
	float ShotDamage = 0.5f;

public:
	ATDS_TurretPawn();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void Death();

	UFUNCTION()
	void TakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void ShowStatus(ATDS_HUD* HUD) const override;
};
