// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_GoldMine.h"
#include "../../UI/TDS_HUD.h"
#include "../TDS_HealthSystem.h"
#include "../TDS_DamageTypes.h"
#include "TDS_GoldNugget.h"

ATDS_GoldMine::ATDS_GoldMine()
{
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetCollisionProfileName("BlockAll");

	HealthSystem = CreateDefaultSubobject<UTDS_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(2.f);

	HealthSystem->OnDied.AddDynamic(this, &ATDS_GoldMine::DigGold);
	OnTakePointDamage.AddDynamic(this, &ATDS_GoldMine::TakePointDamage);
}

void ATDS_GoldMine::BeginPlay()
{
	Super::BeginPlay();
}

void ATDS_GoldMine::TakePointDamage(AActor* DamagedActor, float Damage, 
	AController* InstigatedBy, FVector HitLocation, 
	UPrimitiveComponent* FHitComponent, FName BoneName, 
	FVector ShotFromDirection, const UDamageType* DamageType, 
	AActor* DamageCauser)
{
	if (DamageType->GetClass() == UTDS_DrillDamage::StaticClass())
	{
		LastShotLoc = HitLocation;
		LastShotDir = ShotFromDirection;
		HealthSystem->ReduceHP(Damage);
	}
}

void ATDS_GoldMine::DigGold()
{
	HealthSystem->RestoreFullHP();

	FActorSpawnParameters asp;
	asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	GetWorld()->SpawnActor<ATDS_GoldNugget>(GoldNuggetClass, FTransform(LastShotLoc), asp);
}

void ATDS_GoldMine::ShowStatus(ATDS_HUD* HUD) const
{
	if (HUD != nullptr)
	{
		FString s = FString::Printf(TEXT("Gold Mine\nHP: %.2f"), HealthSystem->GetCurHP());
		HUD->DrawStatus(s);
	}
}

