// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../UI/TDS_ActorStatus.h"
#include "TDS_GoldNugget.generated.h"

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_GoldNugget : public AActor, public ITDS_ActorStatus
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* Mesh;

public:	
	// Sets default values for this actor's properties
	ATDS_GoldNugget();

	UFUNCTION()
	void ShowStatus(ATDS_HUD* HUD) const override;
	UFUNCTION()
	void ShowAction(ATDS_HUD* HUD) const override;
	UFUNCTION()
	void ActivateAction(APawn* Operator) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
