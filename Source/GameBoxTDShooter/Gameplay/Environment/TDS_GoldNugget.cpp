// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_GoldNugget.h"
#include "../../UI/TDS_HUD.h"
#include "../TDS_Character.h"

// Sets default values
ATDS_GoldNugget::ATDS_GoldNugget()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetCollisionProfileName("Resource");
	SetRootComponent(Mesh);
	Mesh->SetSimulatePhysics(true);

}

void ATDS_GoldNugget::ShowStatus(ATDS_HUD* HUD) const
{
	if (HUD != nullptr)
	{
		HUD->DrawStatus(TEXT("Gold Nugget"));
	}
}

void ATDS_GoldNugget::ShowAction(ATDS_HUD* HUD) const
{
	if (HUD != nullptr)
	{
		HUD->DrawAction(TEXT("Press [E] to Pick Up"));
	}
}

void ATDS_GoldNugget::ActivateAction(APawn* Operator)
{
	ATDS_Character* ch = Cast<ATDS_Character>(Operator);

	if (IsValid(ch))
	{
		ch->TakeGold(1);
		Destroy();
	}
}

// Called when the game starts or when spawned
void ATDS_GoldNugget::BeginPlay()
{
	Super::BeginPlay();
	
}

