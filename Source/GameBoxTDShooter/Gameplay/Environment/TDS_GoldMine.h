// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../UI/TDS_ActorStatus.h"
#include "TDS_GoldMine.generated.h"

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_GoldMine : public AActor, public ITDS_ActorStatus
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere)
	class UTDS_HealthSystem* HealthSystem;

	float GoldNuggetSpawnDistance = 100;
	FVector LastShotLoc;
	FVector LastShotDir;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ATDS_GoldNugget> GoldNuggetClass;

public:	
	ATDS_GoldMine();

protected:
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void TakePointDamage (AActor* DamagedActor, float Damage,
		class AController* InstigatedBy, 
		FVector HitLocation, 
		class UPrimitiveComponent* FHitComponent, 
		FName BoneName, 
		FVector ShotFromDirection, 
		const class UDamageType* DamageType, 
		AActor* DamageCauser);

	UFUNCTION()
	void DigGold();

public:	

	UFUNCTION()
	void ShowStatus(ATDS_HUD* HUD) const override;
};
