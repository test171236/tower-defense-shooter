// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_EnemySpawner.generated.h"

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_EnemySpawner : public AActor
{
	GENERATED_BODY()
	
	float LastSpawnTime;

	UPROPERTY(EditAnywhere)
	float SpawnRate = 0.01f;
	UPROPERTY(EditAnywhere)
	float SpawnRateAcceleration = 0.001f;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATDS_EnemyCharacter> EnemyClass;
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnemySpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
