// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnemyCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "TDS_EnemyController.h"
#include "../TDS_Character.h"
#include "../TDS_HealthSystem.h"
#include "../TDS_DamageTypes.h"
#include "../../UI/TDS_HUD.h"

// Sets default values
ATDS_EnemyCharacter::ATDS_EnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AttackCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("AttackCollision"));
	AttackCollision->SetCollisionProfileName(TEXT("NoCollision"));
	AttackCollision->AttachToComponent(GetMesh(),
		FAttachmentTransformRules::KeepRelativeTransform, FName("LeftHandSocket"));

	AttackCollision->OnComponentBeginOverlap.AddDynamic(this, &ATDS_EnemyCharacter::OnAttackOverlap);

	HealthSystem = CreateDefaultSubobject<UTDS_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(5.f);
	
	HealthSystem->OnDied.AddDynamic(this, &ATDS_EnemyCharacter::Death);
	OnTakePointDamage.AddDynamic(this, &ATDS_EnemyCharacter::TakePointDamage);
}

// Called when the game starts or when spawned
void ATDS_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ATDS_EnemyCharacter::OnAttackOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isAttacking && OverlappedComponent == AttackCollision)
		UGameplayStatics::ApplyDamage(OtherActor, 1., GetController(), this, UTDS_MeleeDamage::StaticClass());
}

void ATDS_EnemyCharacter::Death()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	AnimInstance->OnMontageEnded.Clear();
	AnimInstance->OnPlayMontageNotifyBegin.Clear();
	AnimInstance->OnPlayMontageNotifyEnd.Clear();
	AnimInstance->StopAllMontages(0);

	isDead = true;
	isAttacking = false;
	AttackCollision->SetCollisionProfileName(TEXT("NoCollision"));
	
	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

	GetMesh()->SetCollisionProfileName(TEXT("Ragdoll"));
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;

	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (CharacterComp)
	{
		CharacterComp->StopMovementImmediately();
		CharacterComp->DisableMovement();
		CharacterComp->SetComponentTickEnabled(false);
	}

	AAIController* cont = Cast<AAIController>(GetController());
	if (cont)
		cont->StopMovement();

	SetLifeSpan(10.0f);
}

void ATDS_EnemyCharacter::TakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
	if (DamageType->GetClass() == UTDS_DrillDamage::StaticClass() ||
		DamageType->GetClass() == UTDS_HitScanDamage::StaticClass())
		HealthSystem->ReduceHP(Damage);
}

void ATDS_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector v = GetVelocity();

	if (v.SizeSquared() > 0)
		SetActorRotation(FRotator(0, v.Rotation().Yaw, 0));
}

// Called to bind functionality to input
void ATDS_EnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATDS_EnemyCharacter::SetIsAttacking(bool newIsAttacking)
{
	isAttacking = newIsAttacking;
	if (isAttacking)
		AttackCollision->SetCollisionProfileName(TEXT("OverlapAllDynamic"));
	else 
		AttackCollision->SetCollisionProfileName(TEXT("NoCollision"));
}

void ATDS_EnemyCharacter::ShowStatus(ATDS_HUD* HUD) const
{
	if (HUD != nullptr)
	{
		FString s = FString::Printf(TEXT("Zombie\nHP: %.2f"), HealthSystem->GetCurHP());
		HUD->DrawStatus(s);
	}
}
