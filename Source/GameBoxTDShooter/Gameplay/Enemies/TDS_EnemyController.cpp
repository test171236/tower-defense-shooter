// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnemyController.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "Math/UnrealMathUtility.h"

#include "TDS_EnemyCharacter.h"
#include "../../Game/TDS_GameModeBase.h"

void ATDS_EnemyController::BeginPlay()
{
    Super::BeginPlay();

    PrimaryActorTick.bCanEverTick = false;

    FindTarget();
    if (IsValid(CurrentTarget))
        MoveToActor(CurrentTarget, AttackRadius);
}

void ATDS_EnemyController::FindTarget()
{
    if (FMath::RandBool())
        CurrentTarget = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
    else {
        ATDS_GameModeBase* gm = Cast<ATDS_GameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
        if (IsValid(gm))
        {
            auto& arr = gm->GetAllTurrets();
            if (arr.Num() > 0)
                CurrentTarget = arr[FMath::Rand() % arr.Num()];
            else
                CurrentTarget = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
        }
    }
}

void ATDS_EnemyController::StopIdle()
{
    if (IsValid(CurrentTarget))
        MoveToActor(CurrentTarget, AttackRadius);
}

void ATDS_EnemyController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

}

void ATDS_EnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
    Super::OnMoveCompleted(RequestID, Result);

    if (Result.IsSuccess())
        AttackStart();
    else
    {
        FindTarget();
        if (IsValid(CurrentTarget))
        {
            FTimerHandle handle;
            GetWorldTimerManager().SetTimer(
                handle, this, &ATDS_EnemyController::StopIdle, DelayTime, false);
        }
    }
}

void ATDS_EnemyController::AttackWindowStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(GetPawn());
    if (IsValid(ch))
    {
        ch->SetIsAttacking(true);
    }
}

void ATDS_EnemyController::AttackWindowEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
    ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(GetPawn());
    if (IsValid(ch))
    {
        ch->SetIsAttacking(false);
    }
}

void ATDS_EnemyController::AttackStart()
{
    ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(GetPawn());
    if (IsValid(ch) && IsValid(ch->GetMesh()))
    {
        if (IsValid(CurrentTarget))
        {
            ch->PlayAnimMontage(ch->AttackMontage);

            UAnimInstance* AnimInstance = ch->GetMesh()->GetAnimInstance();
            AnimInstance->OnMontageEnded.AddDynamic(this, &ATDS_EnemyController::AttackEnd);
            AnimInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &ATDS_EnemyController::AttackWindowStart);
            AnimInstance->OnPlayMontageNotifyEnd.AddDynamic(this, &ATDS_EnemyController::AttackWindowEnd);

            FVector delta = CurrentTarget->GetActorLocation() - ch->GetActorLocation();
            ch->SetActorRotation(FRotator(0, delta.Rotation().Yaw, 0));
        }
        else
        {
            FindTarget();
            if (IsValid(CurrentTarget))
                MoveToActor(CurrentTarget, AttackRadius);
        }
    }
}

void ATDS_EnemyController::AttackEnd(UAnimMontage* Montage, bool bInterrupted)
{
    ATDS_EnemyCharacter* ch = Cast<ATDS_EnemyCharacter>(GetPawn());

    if (IsValid(ch) && IsValid(ch->GetMesh()))
    {
        UAnimInstance* AnimInstance = ch->GetMesh()->GetAnimInstance();
        AnimInstance->OnMontageEnded.Clear();
        AnimInstance->OnPlayMontageNotifyBegin.Clear();
        AnimInstance->OnPlayMontageNotifyEnd.Clear();
    }

    if (!bInterrupted)
        if (IsValid(CurrentTarget))
            MoveToActor(CurrentTarget, AttackRadius);
        else
        {
            FindTarget();
            if (IsValid(CurrentTarget))
                MoveToActor(CurrentTarget, AttackRadius);
        }
}
