// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnemySpawner.h"
#include "TDS_EnemyCharacter.h"

ATDS_EnemySpawner::ATDS_EnemySpawner()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ATDS_EnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	
	LastSpawnTime = GetWorld()->GetTimeSeconds();
}

void ATDS_EnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SpawnRate += SpawnRateAcceleration * DeltaTime;

	float cur_t = GetWorld()->GetTimeSeconds();
	if (cur_t - LastSpawnTime >= 1.f / SpawnRate)
	{
		FVector spawn_loc = GetActorLocation();

		GetWorld()->SpawnActor<ATDS_EnemyCharacter>(EnemyClass, FTransform(spawn_loc));

		LastSpawnTime = cur_t;
	}
}

