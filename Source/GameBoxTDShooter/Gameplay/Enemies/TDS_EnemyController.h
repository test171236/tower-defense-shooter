// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TDS_EnemyController.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_EnemyController : public AAIController
{
	GENERATED_BODY()

	UPROPERTY()
	AActor* CurrentTarget = nullptr;

	float AttackRadius = 50;
	float DelayTime = 0.5;

protected:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;


	UFUNCTION()
	void AttackWindowStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackWindowEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);

	UFUNCTION()
	void AttackStart();
	UFUNCTION()
	void AttackEnd(UAnimMontage* Montage, bool bInterrupted);

	void FindTarget();
	void StopIdle();
public:
};
