// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../../UI/TDS_ActorStatus.h"
#include "TDS_EnemyCharacter.generated.h"

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_EnemyCharacter : public ACharacter, public ITDS_ActorStatus
{
	GENERATED_BODY()

	bool isAttacking = false;

	UPROPERTY(EditDefaultsOnly)
	class UCapsuleComponent* AttackCollision;

	UPROPERTY(EditDefaultsOnly)
	class UTDS_HealthSystem* HealthSystem;

	bool isDead = false;

public:
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* AttackMontage;

	// Sets default values for this character's properties
	ATDS_EnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnAttackOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void Death();

	UFUNCTION()
	void TakePointDamage(AActor* DamagedActor, float Damage,
		class AController* InstigatedBy,
		FVector HitLocation,
		class UPrimitiveComponent* FHitComponent,
		FName BoneName,
		FVector ShotFromDirection,
		const class UDamageType* DamageType,
		AActor* DamageCauser);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void SetIsAttacking(bool newIsAttacking);

	virtual void ShowStatus(ATDS_HUD* HUD) const override;

	bool IsDead() const { return isDead; }
};
