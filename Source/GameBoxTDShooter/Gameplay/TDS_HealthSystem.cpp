// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HealthSystem.h"

UTDS_HealthSystem::UTDS_HealthSystem()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UTDS_HealthSystem::BeginPlay()
{
	Super::BeginPlay();
}

void UTDS_HealthSystem::SetMaxHP(float newMaxHP)
{
	MaxHP = newMaxHP;
	CurHP = newMaxHP;
	isDead = false;
}

float UTDS_HealthSystem::ReduceHP(float deltaHP)
{
	float res = 0;

	if (CurHP > deltaHP)
	{
		res = deltaHP;
		CurHP -= deltaHP;
	}
	else if (!isDead)
	{
		isDead = true;
		res = CurHP;
		CurHP = 0.f;
		OnDied.Broadcast();
	}

	return res;
}

void UTDS_HealthSystem::RestoreFullHP()
{
	isDead = false;
	CurHP = MaxHP;
}

float UTDS_HealthSystem::GetCurHP()	const
{
	return CurHP;
}

float UTDS_HealthSystem::GetMaxHP()	const
{
	return MaxHP;
}

