#include "TDS_Character.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "TDS_Projectile.h"
#include "../UI/TDS_ActorStatus.h"
#include "../UI/TDS_HUD.h"
#include "TDS_DamageTypes.h"
#include "TDS_HealthSystem.h"
#include "Turret/TDS_TurretPawn.h"
#include "Turret/TDS_TurretPlacement.h"
#include "../UI/TDS_MenuWidget.h"

ATDS_Character::ATDS_Character()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);		
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	HealthSystem = CreateDefaultSubobject<UTDS_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(10.f);
	HealthSystem->OnDied.AddDynamic(this, &ATDS_Character::Death);
	OnTakeAnyDamage.AddDynamic(this, &ATDS_Character::TakeAnyDamage);
}

void ATDS_Character::BeginPlay()
{
	Super::BeginPlay();
	

	APlayerController* contr = Cast<APlayerController>(GetController());
	if (IsValid(contr))
	{
		MenuWidget = CreateWidget<UTDS_MenuWidget>(contr, MenuWidgetClass);
		MenuWidget->RemoveFromViewport();
	}

	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	DrillBeamComp = UNiagaraFunctionLibrary::SpawnSystemAttached(
		DrillBeamSystem,
		FP_MuzzleLocation,
		FName(),
		FP_MuzzleLocation->GetComponentLocation(),
		FRotator::ZeroRotator,
		EAttachLocation::SnapToTarget,
		false,
		false,
		ENCPoolMethod::ManualRelease
	);
	DrillBeamComp->SetRelativeLocation(FVector::ZeroVector);
}

void ATDS_Character::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	DrillBeamComp->DestroyComponent();
	MenuWidget->Destruct();
}

void ATDS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult look_hr;

	FVector look_dir = FirstPersonCameraComponent->GetForwardVector(),
		look_start = FirstPersonCameraComponent->GetComponentLocation(),
		look_end = look_start + look_dir * VisionRadius;

	UKismetSystemLibrary::LineTraceSingle(
		GetWorld(),
		look_start,
		look_end,
		ETraceTypeQuery::TraceTypeQuery1,
		false,
		TArray<AActor*>(),
		EDrawDebugTrace::None,
		look_hr,
		true
	);

	InteractableActor = nullptr;

	if (look_hr.bBlockingHit)
	{
		ATDS_HUD* hud = Cast<ATDS_HUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
		ITDS_ActorStatus* status = Cast<ITDS_ActorStatus>(look_hr.Actor);
		
		if (hud != nullptr && status != nullptr)
		{
			status->ShowStatus(hud);
			if (look_hr.Distance <= ActionDistance)
			{
				status->ShowAction(hud);
				InteractableActor = status;
			}
		}

	}

	if (IsPlacingTurret)
	{
		PlacingTurret->SetActorLocation(look_hr.Location);
		PlacingTurret->SetCanBePlaced(look_hr.bBlockingHit
			&& look_hr.ImpactNormal.Z > 0.7
			&& PlacingTurret->GetOverlapCount() == 0
			&& CurrentGold >= TurretPlacementCost);
	}
	else if (IsFiring)
	{
		FHitResult fire_hr;
		FVector fire_start = FP_MuzzleLocation->GetComponentLocation();
		FVector fire_end = look_hr.bBlockingHit ? look_hr.Location : look_end;
		FVector fire_dir = (fire_end - fire_start).GetSafeNormal();

		fire_end = fire_start + fire_dir * MaxFireBeamLen;

		UKismetSystemLibrary::LineTraceSingle(
			GetWorld(),
			fire_start,
			fire_end,
			ETraceTypeQuery::TraceTypeQuery1,
			false,
			TArray<AActor*>(),
			EDrawDebugTrace::ForOneFrame,
			fire_hr,
			true
		);

		if (fire_hr.bBlockingHit)
		{
			UGameplayStatics::ApplyPointDamage(fire_hr.GetActor(), 
				DeltaTime,
				fire_dir, 
				fire_hr, 
				GetController(),
				this,
				UTDS_DrillDamage::StaticClass());
			DrillBeamComp->SetVectorParameter("EndPoint", fire_hr.ImpactPoint);
		}
		else
		{
			DrillBeamComp->SetVectorParameter("EndPoint", fire_end);
		}
		
	}
}

void ATDS_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATDS_Character::OnFireStart);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ATDS_Character::OnFireEnd);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ATDS_Character::OnInteract);

	PlayerInputComponent->BindAction("PlaceTurret", IE_Pressed, this, &ATDS_Character::OnPlaceTurret);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATDS_Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATDS_Character::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	ATDS_HUD* hud = Cast<ATDS_HUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
	if (IsValid(hud))
		PlayerInputComponent->BindAction("ToggleHint", IE_Pressed, hud, &ATDS_HUD::ToggleHint);
}

void ATDS_Character::OnFireStart()
{
	if (IsPlacingTurret)
	{
		if (PlacingTurret->GetCanBePlaced())
		{
			SpendGold(TurretPlacementCost);

			FVector spawn_loc = PlacingTurret->GetActorLocation();
			OnPlaceTurret();

			GetWorld()->SpawnActor<ATDS_TurretPawn>(TurretClass, FTransform(spawn_loc));
		}

		return;
	}

	IsFiring = true;
	DrillBeamComp->Activate(true);

	if (FireAnimation != nullptr)
	{
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void ATDS_Character::OnFireEnd()
{
	IsFiring = false;
	DrillBeamComp->Deactivate();
}

void ATDS_Character::OnInteract()
{
	if (InteractableActor != nullptr)
		InteractableActor->ActivateAction(this);
}

void ATDS_Character::OnPlaceTurret()
{
	IsPlacingTurret = !IsPlacingTurret;

	if (IsPlacingTurret)
	{
		PlacingTurret = GetWorld()->SpawnActor<ATDS_TurretPlacement>(TurretPlacementClass);
		OnFireEnd();
	}
	else
		PlacingTurret->Destroy();
}

void ATDS_Character::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATDS_Character::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ATDS_Character::Death(void)
{
	APlayerController* contr = Cast<APlayerController>(GetController());
	if (IsValid(contr))
	{
		MenuWidget->AddToViewport(1);
		contr->bShowMouseCursor = true;
		contr->StopMovement();
		contr->InputComponent->bBlockInput = true;
	}
}

void ATDS_Character::TakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (DamageType->GetClass() == UTDS_MeleeDamage::StaticClass())
		HealthSystem->ReduceHP(Damage);
}

bool ATDS_Character::SpendGold(int goldAmount)
{
	if (goldAmount > CurrentGold)
		return false;

	CurrentGold -= goldAmount;
	return true;
}


