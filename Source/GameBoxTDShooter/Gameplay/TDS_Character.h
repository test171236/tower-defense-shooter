// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS_HealthSystem.h"
#include "TDS_Character.generated.h"

class UCameraComponent;

UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_Character : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* FP_MuzzleLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(EditDefaultsOnly)
	class UNiagaraSystem* DrillBeamSystem;
	UPROPERTY()
	class UNiagaraComponent* DrillBeamComp;

	UPROPERTY()
	UTDS_HealthSystem* HealthSystem;

	UPROPERTY()
	class UTDS_MenuWidget* MenuWidget;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDS_MenuWidget> MenuWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATDS_TurretPawn> TurretClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATDS_TurretPlacement> TurretPlacementClass;

	class ITDS_ActorStatus* InteractableActor = nullptr;

	UPROPERTY()
	bool IsFiring = false;
	UPROPERTY()
	float MaxFireBeamLen = 1000;
	UPROPERTY()
	float ActionDistance = 200;
	UPROPERTY()
	int CurrentGold = 0;

	bool IsPlacingTurret = false;
	
	UPROPERTY()
	class ATDS_TurretPlacement* PlacingTurret = nullptr;

	float VisionRadius = 10000;

	int TurretPlacementCost = 5;

public:
	// Sets default values for this character's properties
	ATDS_Character();

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void OnFireStart();
	void OnFireEnd();
	void OnInteract();
	void OnPlaceTurret();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	UFUNCTION()
	void Death();

	UFUNCTION()
	void TakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	bool SpendGold(int goldAmount);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void TakeGold(int GoldAmount) { CurrentGold += GoldAmount;}

	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	float GetCurrentHP() const { return HealthSystem->GetCurHP() / HealthSystem->GetMaxHP(); }
	int GetCurrentGold() const { return CurrentGold; }
};
