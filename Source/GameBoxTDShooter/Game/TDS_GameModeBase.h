// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "../Gameplay/Turret/TDS_TurretPawn.h"
#include "TDS_GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOXTDSHOOTER_API ATDS_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<ATDS_TurretPawn*> AllTurrets;

public:
	ATDS_GameModeBase();

	void AddTurret(ATDS_TurretPawn* NewTurret);
	void RemoveTurret(ATDS_TurretPawn* TurretToRemove);

	const TArray<ATDS_TurretPawn*>& GetAllTurrets() const
	{return AllTurrets;}
};
