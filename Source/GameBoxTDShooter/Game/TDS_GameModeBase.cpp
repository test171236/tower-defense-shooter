// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_GameModeBase.h"
#include "UObject/ConstructorHelpers.h"
#include "../UI/TDS_HUD.h"
#include "../Gameplay/TDS_Character.h"

ATDS_GameModeBase::ATDS_GameModeBase()
	: Super()
{
	AllTurrets.Empty();
}

void ATDS_GameModeBase::AddTurret(ATDS_TurretPawn* NewTurret)
{
	AllTurrets.AddUnique(NewTurret);
}

void ATDS_GameModeBase::RemoveTurret(ATDS_TurretPawn* TurretToRemove)
{
	AllTurrets.Remove(TurretToRemove);
}
